package menu;

public class TestdbRecibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Verificando errores");
        dbRecibo rec= new dbRecibo();
        rec.conectar();
        
        recibo examen02 = new recibo();
        examen02.setNumrecibo(10);
        examen02.setNombre("jose miguel");
        examen02.setDomicilio("av del sol 33");
        examen02.setTipo(1);
        examen02.setCostos(10f);
        examen02.setConsumo(11f);
        examen02.setFecha("2023-12-08");
        
        
        // rec.insertar(recibo);
        rec.actualizar(examen02);
        
        rec.deshabilitar(examen02);
        if(rec.isExiste(100,0)) System.out.println("si existe");
        else System.out.println("no existe");
        rec.habilitar(examen02);
        if(rec.isExiste(100,0)) System.out.println("si existe");
        else System.out.println("no existe");
        
        recibo result= rec.buscar(100);
        
        if(result.getId()>0 ){
            System.out.println(result.getNombre()+ "dom" + result.getDomicilio() ); 
        }
    }
    
}
