
package menu;

public class recibo {
    
    private int id;
    private int numrecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipo;
    private float costos;
    private float consumo;
    private int status;
  
    
    public recibo(){
    this.id=0;
    this.numrecibo=0;
    this.nombre="";
    this.domicilio="";
    this.tipo=0;
    this.costos=0.0f;
    this.fecha="";
    this.consumo=0.0f;
    this.status=0;
    
    
    }
    //Constructor por argumentos
    public recibo(int id, int numrecibo, String nombre, String domicilio, int tipo, float costos, String fecha, int consumo, int status){
    this.id=id;
    this.numrecibo=numrecibo;
    this.nombre=nombre;
    this.domicilio=domicilio;
    this.tipo=tipo;
    this.costos=costos;
    this.fecha=fecha;
    this.consumo=consumo;
    this.status=status;
   
    }
    
    //Copia "OTRO"
    public recibo(recibo otro){
    this.id=otro.id;
    this.numrecibo=otro.numrecibo;
    this.nombre=otro.nombre;
    this.domicilio=otro.domicilio;
    this.tipo=otro.tipo;
    this.costos=otro.costos;
    this.fecha=otro.fecha;
    this.consumo=otro.consumo;
    this.status=otro.status;
    }
    
    //Metodos Set y Get
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumrecibo() {
        return numrecibo;
    }

    public void setNumrecibo(int numrecibo) {
        this.numrecibo = numrecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCostos() {
        return costos;
    }

    public void setCostos(float costos) {
        this.costos = costos;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
     

    
    //Metodos de comportamiento

     public float calcularSubtotal(){
    float subtotal=0.0f;
    if (this.tipo == 1) {
        subtotal=(this.consumo*this.costos);
    }
    if (this.tipo == 2) {
        subtotal=(this.consumo*this.costos);
    }
    if (this.tipo == 3) {
         subtotal=(this.consumo*this.costos);
    }
    return subtotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubtotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubtotal()+this.calcularImpuesto();
    return total;
    }
    
}


    
    
