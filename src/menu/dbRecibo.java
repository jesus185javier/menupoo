package menu;

import java.sql.*;
public class dbRecibo {
    
    
    private String MYSQLDRIVER="com.mysql.cj.jdbc.Driver";
    private String MYSQLDB="jdbc:mysql://3.132.136.208:3306/jesus?user=jesus&password=jesus185";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    //constructor
    public dbRecibo(){
        
        try {
            Class.forName(MYSQLDRIVER);
            
        } catch( ClassNotFoundException e){
        
            System.out.println("Surgio un ERROR"+ e.getMessage());
            System.exit(-1);
            
        }
        
    }
       public void conectar(){
            
        try {
        conexion = DriverManager.getConnection(MYSQLDB);
        } catch(SQLException e){
            
            System.out.println("No se logró conectar "+e.getMessage());
        }
        
        }
       
       public void desconectar(){
       
       try {
           
       conexion.close();
       
       } catch(SQLException e){
       
       System.out.println("Surgió un error al desconectar "+ e.getMessage());
       }
    
}
       
       public void insertar(recibo rec){
           conectar();
           try{
               
       strConsulta= "INSERT INTO recibo(numrecibo, fecha, nombre, domicilio, tipo, costos, consumo, status) "
               + "VALUES(?,CURDATE(),?,?,?,?,?,?)";
       
            PreparedStatement pst = conexion.prepareStatement(strConsulta);       
            pst.setInt(1, rec.getNumrecibo());
            pst.setString(2, rec.getNombre());
            pst.setString(3, rec.getDomicilio());
            pst.setInt(4, rec.getTipo());
            pst.setFloat(5, rec.getCostos());
            pst.setFloat(6, rec.getConsumo());
            pst.setInt(7, rec.getStatus());
            
            pst.executeUpdate();
           } catch (SQLException e) {
            System.out.println("Error al insertar "+ e.getMessage());
        }
           desconectar();
}
         public void actualizar(recibo rec){
             recibo recibo2 = new recibo();
          
               
       strConsulta= "UPDATE recibo SET nombre = ?,domicilio = ?,fecha = ?,tipo = " +
               "? ,costos = ?,consumo = ? WHERE numrecibo = ? and status =0;";
        this.conectar();
           try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setString(1, rec.getNombre());
            pst.setString(2, rec.getDomicilio());
            pst.setString(3, rec.getFecha());
            pst.setInt(4, rec.getTipo());
            pst.setFloat(5, rec.getCostos());
            pst.setFloat(6, rec.getConsumo());
            pst.setInt(7, rec.getNumrecibo());
            
            pst.executeUpdate();
            this.desconectar();
           } 
           catch (SQLException e) {
            System.out.println("surgio un error al actualizar "+ e.getMessage());
           }
         }
         
         public void habilitar(recibo rec){
             
             String consulta = "";
             strConsulta = "UPDATE recibo SET status = 0 WHERE numrecibo = ?";
             this.conectar();
             try {
                 System.err.println("se conecto");
                 PreparedStatement pst = conexion.prepareStatement(strConsulta);
                 // asignar los valores a la consulta
                 pst.setInt(1, rec.getNumrecibo());
                 
                 pst.executeUpdate();
                 this.desconectar();
             } catch (SQLException e) {
                 System.err.println("surgio un error al habilitar" + e.getMessage());
             }
         }
         
             public void deshabilitar(recibo rec){
             strConsulta = "UPDATE recibo SET status = 1 WHERE numrecibo = ?";
             this.conectar();
             try {
                 System.err.println("se conecto");
                 PreparedStatement pst = conexion.prepareStatement(strConsulta);
                 // asignar los valores a la consulta
                 pst.setInt(1, rec.getNumrecibo());
                 
                 pst.executeUpdate();
                 this.desconectar();
             } catch (SQLException e) {
                 System.err.println("surgio un error al habilitar" + e.getMessage());
             }
         
        }
             
             public boolean isExiste(int numRecibo, int status) {
                 boolean exito = false;
                 this.conectar();
                 strConsulta = "SELECT * FROM recibo WHERE numrecibo = ? and status = ?;";
                 try {
                     PreparedStatement pst = conexion.prepareStatement(strConsulta);
                     pst.setInt(1, numRecibo);
                     pst.setInt(2, status);
                     this.registros = pst.executeQuery();
                     if(this.registros.next()) exito = true;
                 } catch(SQLException e){
                 System.err.println("surgio un error al verificar si existe: " + e.getMessage());
                 
             }
                 this.desconectar();
                 return exito;
}
             public recibo buscar(int numrecibo){
                 recibo recibo2 = new recibo();
                 conectar();
                 try {
                     strConsulta = "SELECT * FROM recibo WHERE numrecibo = ? and status = 0;";
                     PreparedStatement pst = conexion.prepareStatement(strConsulta);
                     
                     pst.setInt(1, numrecibo);
                     this.registros = pst.executeQuery();
                     if(this.registros.next()){
                         
                         recibo2.setId(registros.getInt("id"));
                         recibo2.setNumrecibo(registros.getInt("numRecibo"));
                         recibo2.setNombre(registros.getString("nombre"));
                         recibo2.setDomicilio(registros.getString("domicilio"));
                         recibo2.setTipo(registros.getInt("tipo"));
                         recibo2.setCostos(registros.getFloat("costos"));
                         recibo2.setConsumo(registros.getFloat("consumo"));
                         recibo2.setFecha(registros.getString("fecha"));
                         
                         
                     } else recibo2.setId(0);
                 }
                 catch(SQLException e){
                     System.err.println("surgio un error al habilitar: " + e.getMessage());
                     
                 }
                 this.desconectar();
                 return recibo2;
             }
}

